**Translation files for the Just Order project**


| Title | Video | Subtitle |
| ------ | ------ | ------ |
| The Just Order Conference - Ankara 1995 - Prof. Dr. Necmettin Erbakan | [video](https://www.youtube.com/watch?v=If1wwAfspN4) | [Link](https://gitlab.com/halalorder/the-just-order/-/blob/master/The-Just-Economic-System-Conference-Ankara-1991.srt) |

**For donations:**

**Bitcoin (BTC)**

`12DrsXBTm5VY5bFp7F9vdLXCqU4i75UkH2`

**ETH (Etherum)**

`0x64930A4a61d0519023ea2e0F73b6F393a8738A20`

**Bitcoin Cash (BCH)**

`qqxkd0mqlplzn3kmaztvyepl28w0j07r8qyz3ut62s`